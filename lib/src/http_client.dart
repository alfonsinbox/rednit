import 'dart:io';

import 'package:http/http.dart' as http;

import 'auth/credentials.dart';

class RedditHttpClient extends http.BaseClient {
  final http.Client _client;
  final Credentials _credentials;

  RedditHttpClient(this._client, this._credentials);

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    request.headers[HttpHeaders.userAgentHeader] = _credentials.userAgent;
    return _client.send(request);
  }

  @override
  void close() {
    _client.close();
  }
}
