abstract class Credentials {
  final String clientId;
  final String creatorUsername;
  final String appVersion;

  String get userAgent;
  String get basicCredentials;

  const Credentials({
    required this.clientId,
    required this.creatorUsername,
    required this.appVersion,
  });
}

class InstalledAppCredentials extends Credentials {
  final String redirectUri;
  final String packageId;
  final String platformName;

  @override
  String get userAgent =>
      '$platformName:$packageId:v$appVersion (by /u/$creatorUsername)';

  @override
  String get basicCredentials => '$clientId:';

  InstalledAppCredentials({
    required this.redirectUri,
    required this.packageId,
    required this.platformName,
    required String clientId,
    required String creatorUsername,
    required String appVersion,
  }) : super(
          clientId: clientId,
          creatorUsername: creatorUsername,
          appVersion: appVersion,
        );
}

class ScriptAppCredentials extends Credentials {
  final String clientSecret;
  final String username;
  final String password;
  final String appName;

  @override
  String get userAgent => '$appName/v$appVersion (by /u/$creatorUsername)';

  @override
  String get basicCredentials => '$clientId:$clientSecret';

  ScriptAppCredentials({
    required this.clientSecret,
    required this.username,
    required this.password,
    required this.appName,
    required String clientId,
    required String creatorUsername,
    required String appVersion,
  }) : super(
          clientId: clientId,
          creatorUsername: creatorUsername,
          appVersion: appVersion,
        );
}
