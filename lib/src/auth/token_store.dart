import 'dart:async';

abstract class TokenStore {
  // Getters for token store
  String? get accessToken;
  String? get refreshToken;
  DateTime? get accessTokenExpiration;
  String? get deviceId;

  // Setters for token store
  Future<void> setAccessToken(String accessToken);
  Future<void> setRefreshToken(String refreshToken);
  Future<void> setAccessTokenExpiration(DateTime accessTokenExpiration);
  Future<void> setDeviceId(String deviceId);

  // Removers for token store
  Future<void> removeAccessToken();
  Future<void> removeRefreshToken();
  Future<void> removeAccessTokenExpiration();
}
