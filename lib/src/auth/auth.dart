import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:uuid/uuid.dart';
import 'package:http/http.dart' as http;

import 'credentials.dart';
import 'token_store.dart';
import '../http_client.dart';

class AuthManager {
  final http.Client _httpClient;
  final TokenStore _tokenStore;
  final Credentials _credentials;
  static const Duration renewMargin = Duration(minutes: 5);

  String? _state;

  AuthManager(this._tokenStore, http.Client httpClient, this._credentials)
      : _httpClient = RedditHttpClient(httpClient, _credentials);

  Future<bool> get isSignedIn async {
    return _tokenStore.refreshToken != null;
  }

  Future<bool> get needsRenew async {
    final expiration = _tokenStore.accessTokenExpiration;
    if (expiration != null) {
      final timeTilExpiration = expiration.difference(DateTime.now());
      return timeTilExpiration < renewMargin;
    }
    return true;
  }

  String getLoginUrl(List<String> scopes) {
    if (_credentials is! InstalledAppCredentials) {
      return '';
    }
    _state = Uuid().v4();

    final loginPage = Uri.https('old.reddit.com', '/api/v1/authorize.compact', {
      'client_id': '${_credentials.clientId}',
      'redirect_uri':
          '${(_credentials as InstalledAppCredentials).redirectUri}',
      'state': '$_state',
      'response_type': 'code',
      'duration': 'permanent',
      'scope': scopes.join(','),
    });
    return loginPage.toString();
  }

  Future<void> completeAuthorizationCodeFlow({
    required String state,
    required String code,
  }) async {
    if (_credentials is! InstalledAppCredentials) return;

    if (state != _state) {
      throw StateError('Auth state returned does not match last state');
    }
    final basicCredentials =
        base64.encode(utf8.encode(_credentials.basicCredentials));
    final accessTokenResponse = await _httpClient.post(
      Uri.https('www.reddit.com', '/api/v1/access_token'),
      body: {
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': (_credentials as InstalledAppCredentials).redirectUri,
      },
      headers: {
        HttpHeaders.authorizationHeader: 'Basic $basicCredentials',
      },
    );
    if (accessTokenResponse.statusCode != 200) {
      throw Exception('Got status code ${accessTokenResponse.statusCode}');
    }
    final responseBody =
        json.decode(accessTokenResponse.body) as Map<String, dynamic>;
    if (responseBody.containsKey('error')) {
      throw Exception(responseBody['error']);
    }
    await _tokenStore.setAccessToken(responseBody['access_token'] as String);
    await _tokenStore.setRefreshToken(responseBody['refresh_token'] as String);
    await _tokenStore.setAccessTokenExpiration(
      DateTime.now().add(Duration(seconds: responseBody['expires_in'] as int)),
    );
  }

  Future<void> renewAccessToken() async {
    final basicCredentials =
        base64.encode(utf8.encode(_credentials.basicCredentials));

    final accessTokenResponse = await _httpClient.post(
      Uri.https('www.reddit.com', '/api/v1/access_token'),
      body: {
        if (_credentials is InstalledAppCredentials) ...{
          if (_tokenStore.refreshToken != null) ...{
            'grant_type': 'refresh_token',
            'refresh_token': _tokenStore.refreshToken,
          },
          if (_tokenStore.refreshToken == null) ...{
            'grant_type': 'https://oauth.reddit.com/grants/installed_client',
            'device_id': _tokenStore.deviceId,
          }
        },
        if (_credentials is ScriptAppCredentials) ...{
          'grant_type': 'password',
          'username': (_credentials as ScriptAppCredentials).username,
          'password': (_credentials as ScriptAppCredentials).password,
        }
      },
      headers: {
        HttpHeaders.authorizationHeader: 'Basic $basicCredentials',
      },
    );
    if (accessTokenResponse.statusCode != 200) {
      throw Exception('Got status code ${accessTokenResponse.statusCode}');
    }
    final responseBody =
        json.decode(accessTokenResponse.body) as Map<String, dynamic>;
    if (responseBody.containsKey('error')) {
      throw Exception(responseBody['error']);
    }
    await _tokenStore.setAccessToken(responseBody['access_token'] as String);
    await _tokenStore.setAccessTokenExpiration(
      DateTime.now().add(Duration(seconds: responseBody['expires_in'] as int)),
    );
  }

  Future<String> getOrRefreshAccessToken() async {
    if (await needsRenew) {
      await renewAccessToken();
    }
    final accessToken = _tokenStore.accessToken;
    if (accessToken == null) throw Exception('Failed to get access token');
    return accessToken;
  }

  Future<void> logout() async {
    await Future.wait([
      _tokenStore.removeAccessToken(),
      _tokenStore.removeRefreshToken(),
      _tokenStore.removeAccessTokenExpiration(),
    ]);
  }
}
