import 'dart:async';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';

import 'auth/auth.dart';
import 'auth/credentials.dart';
import 'auth/token_store.dart';
import 'http_client.dart';
import 'listing_type.dart';
import 'model/comment.dart';
import 'model/listing.dart';
import 'model/link.dart';
import 'model/subreddit.dart';
import 'model/user.dart';
import 'query.dart';

class RedditClient {
  final http.Client httpClient;
  final TokenStore _tokenStore;
  final AuthManager authManager;

  RedditClient({
    required TokenStore tokenStore,
    required http.Client httpClient,
    required Credentials credentials,
  })   : _tokenStore = tokenStore,
        httpClient = RedditHttpClient(httpClient, credentials),
        authManager = AuthManager(tokenStore, httpClient, credentials) {
    _init();
  }

  /// Initialization of the [RedditClient] run only once, when an instance is created.
  Future _init() async {
    if (_tokenStore.deviceId == null) {
      await _tokenStore.setDeviceId(Uuid().v4());
    }
  }

  /// Dispose the [RedditClient] to release resources. The instance can not be
  /// used after calling [dispose].
  Future<void> dispose() async {
    httpClient.close();
  }

  /// Get information about the currently authenticated user.
  Future<User> getMe() async {
    final query = _makeQuery<Map<String, dynamic>>('/api/v1/me');
    final response = await query.fetch();
    return User.fromJson(response);
  }

  /// Get information about the used with the provided [username].
  Future<User> getUser(String username) async {
    final query = _makeQuery<Map<String, dynamic>>('/user/$username/about');
    final response = await query.fetch();
    return User.fromJson(response['data'] as Map<String, dynamic>);
  }

  /// Get comments posted by the user with the provided [username].
  Future<Listing<Comment>> getUserComments(String username) async {
    return _getListing(path: '/user/$username/comments');
  }

  /// Get submissions posted by the user with the provided [username]. Optionally
  /// set the [after] parameter to get the next page of items.
  Future<Listing<Link>> getUserSubmissions(
    String username, {
    String? after,
  }) async {
    return _getListing(
      path: '/user/$username/submitted',
      after: after,
    );
  }

  /// Get the front page listing sorted by the [listingType]. Optionally set
  /// the [after] parameter to get the next page of items.
  Future<Listing<Link>> getFeed(
    ListingType listingType, {
    String? after,
  }) async {
    return _getListing<Link>(
      path: '/${listingType.path}',
      after: after,
      limit: 10,
    );
  }

  /// Get the listing of submissions for the subreddit with the name [subreddit]
  /// ordered by the [listingType]. Optionally set the [after] parameter to get
  /// the next page of items.
  Future<Listing<Link>> getSubredditFeed(
    String subreddit,
    ListingType listingType, {
    String? after,
  }) async {
    return _getListing<Link>(
      path: '/r/$subreddit/${listingType.path}',
      after: after,
      limit: 10,
    );
  }

  /// Get the comments for the submission with the id [articleName].
  Future<Listing<Comment>> getComments(String articleName) async {
    final query = _makeQuery<List<dynamic>>('/comments/$articleName');
    final response = await query.fetch();
    return Listing<Comment>.fromJson(
        response[1]['data'] as Map<String, dynamic>);
  }

  /// Get details about the subreddit with the name [subredditDisplayName].
  Future<Subreddit> getSubreddit(String subredditDisplayName) async {
    final query = _makeQuery<Map<String, dynamic>>(
      '/r/$subredditDisplayName/about',
    );
    final response = await query.fetch();
    return Subreddit.fromJson(response['data'] as Map<String, dynamic>);
  }

  Future<Listing<Subreddit>> searchSubreddits(String searchQuery) async {
    return _getListing<Subreddit>(
      path: '/subreddits/search',
      searchQuery: searchQuery,
    );
  }

  Future<Listing<T>> _getListing<T>({
    required String path,
    String? after,
    String? before,
    int? limit,
    String? searchQuery,
  }) async {
    if (after != null && before != null) {
      throw Exception('Only one of [after] and [before] can be set.');
    }
    assert(searchQuery?.isNotEmpty ?? true);
    final query = _makeQuery<Map<String, dynamic>>(
      path,
      parameters: {
        if (after != null) 'after': after,
        if (before != null) 'before': before,
        if (limit != null) 'limit': limit.toString(),
        if (searchQuery != null) 'q': searchQuery,
      },
    );
    final response = await query.fetch();
    return Listing<T>.fromJson(response['data'] as Map<String, dynamic>);
  }

  Query<T> _makeQuery<T>(
    String path, {
    Map<String, String> parameters = const {},
  }) {
    return Query<T>(
      path,
      parameters: parameters,
      authManager: authManager,
      httpClient: httpClient,
    );
  }
}
