import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import 'auth/auth.dart';
import 'constants.dart';

class Query<T> {
  final String path;
  final Map<String, String> parameters;
  final http.Client _httpClient;
  final AuthManager _authManager;

  Query(
    this.path, {
    this.parameters = const {},
    required AuthManager authManager,
    required http.Client httpClient,
  })  : _authManager = authManager,
        _httpClient = httpClient;

  Future<T> fetch() async {
    final uri = Uri.https(Constants.baseUriOAuth, '$path.json', {
      'raw_json': '1',
      ...parameters,
    });

    final accessToken = await _authManager.getOrRefreshAccessToken();
    final headers = {
      HttpHeaders.authorizationHeader: 'Bearer $accessToken',
    };
    final response = await _httpClient.get(uri, headers: headers);
    if (response.statusCode != 200) {
      throw Exception(
          'Request for path $path got status code ${response.statusCode}');
    }
    final parsedResponse = json.decode(response.body) as T;
    // if (parsedResponse.containsKey('error')) {
    //   throw Exception(parsedResponse['error']);
    // }
    return parsedResponse;
  }
}
