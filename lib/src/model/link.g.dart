// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'link.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Link _$LinkFromJson(Map<String, dynamic> json) {
  return Link(
    json['ups'] as int,
    json['downs'] as int,
    json['score'] as int,
    json['likes'] as bool,
    json['id'] as String,
    json['name'] as String,
    json['subreddit'] as String,
    json['title'] as String,
    json['selftext'] as String,
    json['num_comments'] as int,
    json['author'] as String,
    json['post_hint'] as String,
    json['thumbnail'] as String,
    json['preview'] == null
        ? null
        : ImagesPreview.fromJson(json['preview'] as Map<String, dynamic>),
    Uri.parse(json['url'] as String),
    json['domain'] as String,
    const SecondsSinceEpochDateParser().fromJson(json['created_utc'] as double),
  );
}

Map<String, dynamic> _$LinkToJson(Link instance) => <String, dynamic>{
      'ups': instance.ups,
      'downs': instance.downs,
      'score': instance.score,
      'likes': instance.likes,
      'id': instance.id,
      'name': instance.name,
      'subreddit': instance.subreddit,
      'title': instance.title,
      'selftext': instance.selftext,
      'num_comments': instance.numComments,
      'author': instance.author,
      'post_hint': instance.postHint,
      'thumbnail': instance.thumbnail,
      'preview': instance.preview,
      'url': instance.url.toString(),
      'domain': instance.domain,
      'created_utc':
          const SecondsSinceEpochDateParser().toJson(instance.createdUtc),
    };

ImagesPreview _$ImagesPreviewFromJson(Map<String, dynamic> json) {
  return ImagesPreview(
    (json['images'] as List<dynamic>?)
        ?.map((e) =>
            e == null ? null : PreviewThing.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['enabled'] as bool,
  );
}

Map<String, dynamic> _$ImagesPreviewToJson(ImagesPreview instance) =>
    <String, dynamic>{
      'images': instance.images,
      'enabled': instance.enabled,
    };

PreviewThing _$PreviewThingFromJson(Map<String, dynamic> json) {
  return PreviewThing(
    PreviewImage.fromJson(json['source'] as Map<String, dynamic>),
    (json['resolutions'] as List<dynamic>)
        .map((e) => PreviewImage.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$PreviewThingToJson(PreviewThing instance) =>
    <String, dynamic>{
      'source': instance.source,
      'resolutions': instance.resolutions,
    };

PreviewImage _$PreviewImageFromJson(Map<String, dynamic> json) {
  return PreviewImage(
    json['url'] as String,
    json['width'] as int,
    json['height'] as int,
  );
}

Map<String, dynamic> _$PreviewImageToJson(PreviewImage instance) =>
    <String, dynamic>{
      'url': instance.url,
      'width': instance.width,
      'height': instance.height,
    };
