// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'listing.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Listing<T> _$ListingFromJson<T>(Map<String, dynamic> json) {
  return Listing<T>(
    (json['children'] as List<dynamic>)
        .map((e) => _ThingConverter<T>().fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ListingToJson<T>(Listing<T> instance) =>
    <String, dynamic>{
      'children': instance.children.map(_ThingConverter<T>().toJson).toList(),
    };
