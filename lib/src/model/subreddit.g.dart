// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subreddit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Subreddit _$SubredditFromJson(Map<String, dynamic> json) {
  return Subreddit(
    json['id'] as String,
    json['name'] as String,
    json['title'] as String,
    json['display_name'] as String,
    json['public_description'] as String,
    json['subscribers'] as int,
  );
}

Map<String, dynamic> _$SubredditToJson(Subreddit instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'title': instance.title,
      'display_name': instance.displayName,
      'public_description': instance.publicDescription,
      'subscribers': instance.subscribers,
    };
