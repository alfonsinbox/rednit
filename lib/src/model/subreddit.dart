import 'package:json_annotation/json_annotation.dart';

import 'mixins/identifiable.dart';

part 'subreddit.g.dart';

@JsonSerializable()
class Subreddit with Identifiable {
  // Identifiable
  @override
  final String id, name;

  // Subreddit specific
  final String title;
  final String displayName;
  final String publicDescription;
  final int subscribers;

  Subreddit(this.id, this.name, this.title, this.displayName,
      this.publicDescription, this.subscribers);

  factory Subreddit.fromJson(Map<String, dynamic> json) =>
      _$SubredditFromJson(json);
}
