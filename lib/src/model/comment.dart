import 'package:json_annotation/json_annotation.dart';
import 'package:rednit/src/model/parsers/date_converter.dart';

import 'mixins/identifiable.dart';

part 'comment.g.dart';

mixin Comment on Identifiable {
  String get parentId;
  int get depth;
}

@JsonSerializable()
class MoreComment implements Comment {
  // Comment
  @override
  final String id, name, parentId;

  @override
  final int depth;

  // MoreComment specific
  final int count;

  MoreComment(this.id, this.name, this.parentId, this.depth, this.count);

  factory MoreComment.fromJson(Map<String, dynamic> json) =>
      _$MoreCommentFromJson(json);
}

@JsonSerializable()
class TextComment implements Comment {
  // Comment
  @override
  final String id, name, parentId;

  @override
  final int depth;

  // TextComment specific
  final String body;
  final String author;

  @SecondsSinceEpochDateParser()
  final DateTime createdUtc;

  @_CommentsParser()
  final List<Comment> replies;

  final int score;

  TextComment(this.id, this.name, this.parentId, this.depth, this.body,
      this.author, this.createdUtc, this.replies, this.score);

  factory TextComment.fromJson(Map<String, dynamic> json) =>
      _$TextCommentFromJson(json);
}

class _CommentsParser implements JsonConverter<List<Comment>, dynamic> {
  const _CommentsParser();

  @override
  List<Comment> fromJson(dynamic json) {
    if (json is String) return [];
    if (json['kind'] as String != 'Listing') {
      throw ArgumentError('JSON is not a Listing');
    }

    final children = json['data']['children'] as List<dynamic>;
    return children.cast<Map<String, dynamic>>().map(_parseCommentMap).toList();
  }

  Comment _parseCommentMap(Map<String, dynamic> commentMap) {
    if (commentMap['kind'] as String == 'more') {
      return MoreComment.fromJson(commentMap['data'] as Map<String, dynamic>);
    }
    // if (commentMap['kind'] as String == 't1') {
    else {
      return TextComment.fromJson(commentMap['data'] as Map<String, dynamic>);
    }
  }

  @override
  dynamic toJson(List<Comment> object) => const {};
}
