// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['name'] as String,
    json['id'] as String,
    json['comment_karma'] as int,
    json['has_mail'] as bool,
    json['has_mod_mail'] as bool,
    json['has_verified_email'] as bool,
    json['inbox_count'] as int,
    json['is_friend'] as bool,
    json['is_gold'] as bool,
    json['link_karma'] as int,
    json['modhash'] as String,
    json['over_18'] as bool,
    const SecondsSinceEpochDateParser().fromJson(json['created'] as double),
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'comment_karma': instance.commentKarma,
      'has_mail': instance.hasMail,
      'has_mod_mail': instance.hasModMail,
      'has_verified_email': instance.hasVerifiedEmail,
      'inbox_count': instance.inboxCount,
      'is_friend': instance.isFriend,
      'is_gold': instance.isGold,
      'link_karma': instance.linkKarma,
      'modhash': instance.modhash,
      'over_18': instance.overEighteen,
      'created': const SecondsSinceEpochDateParser().toJson(instance.created),
    };
