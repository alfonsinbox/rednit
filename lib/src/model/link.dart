import 'package:json_annotation/json_annotation.dart';
import 'package:rednit/src/model/parsers/date_converter.dart';

import 'mixins/identifiable.dart';
import 'mixins/votable.dart';

part 'link.g.dart';

@JsonSerializable()
class Link implements Identifiable, Votable {
  // Votable
  @override
  final int ups, downs, score;
  @override
  final bool likes;

  // Identifiable
  @override
  final String id, name;

  // Link specific
  final String subreddit;
  final String title;
  final String selftext;
  final int numComments;
  final String author;
  final String postHint;
  final String thumbnail;
  final ImagesPreview? preview;
  final Uri url;
  final String domain;

  @SecondsSinceEpochDateParser()
  final DateTime createdUtc;

  Link(
      this.ups,
      this.downs,
      this.score,
      this.likes,
      this.id,
      this.name,
      this.subreddit,
      this.title,
      this.selftext,
      this.numComments,
      this.author,
      this.postHint,
      this.thumbnail,
      this.preview,
      this.url,
      this.domain,
      this.createdUtc);

  factory Link.fromJson(Map<String, dynamic> json) => _$LinkFromJson(json);
}

@JsonSerializable()
class ImagesPreview {
  final List<PreviewThing?>? images;
  final bool enabled;

  ImagesPreview(this.images, this.enabled);

  factory ImagesPreview.fromJson(Map<String, dynamic> json) =>
      _$ImagesPreviewFromJson(json);
}

@JsonSerializable()
class PreviewThing {
  final PreviewImage source;
  final List<PreviewImage> resolutions;

  PreviewThing(this.source, this.resolutions);

  factory PreviewThing.fromJson(Map<String, dynamic> json) =>
      _$PreviewThingFromJson(json);
}

@JsonSerializable()
class PreviewImage {
  final String url;
  final int width, height;

  PreviewImage(this.url, this.width, this.height);

  factory PreviewImage.fromJson(Map<String, dynamic> json) =>
      _$PreviewImageFromJson(json);
}
