// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MoreComment _$MoreCommentFromJson(Map<String, dynamic> json) {
  return MoreComment(
    json['id'] as String,
    json['name'] as String,
    json['parent_id'] as String,
    json['depth'] as int,
    json['count'] as int,
  );
}

Map<String, dynamic> _$MoreCommentToJson(MoreComment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'parent_id': instance.parentId,
      'depth': instance.depth,
      'count': instance.count,
    };

TextComment _$TextCommentFromJson(Map<String, dynamic> json) {
  return TextComment(
    json['id'] as String,
    json['name'] as String,
    json['parent_id'] as String,
    json['depth'] as int,
    json['body'] as String,
    json['author'] as String,
    const SecondsSinceEpochDateParser().fromJson(json['created_utc'] as double),
    const _CommentsParser().fromJson(json['replies']),
    json['score'] as int,
  );
}

Map<String, dynamic> _$TextCommentToJson(TextComment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'parent_id': instance.parentId,
      'depth': instance.depth,
      'body': instance.body,
      'author': instance.author,
      'created_utc':
          const SecondsSinceEpochDateParser().toJson(instance.createdUtc),
      'replies': const _CommentsParser().toJson(instance.replies),
      'score': instance.score,
    };
