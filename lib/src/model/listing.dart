import 'package:json_annotation/json_annotation.dart';

import 'comment.dart';
import 'link.dart';
import 'subreddit.dart';
import 'user.dart';

part 'listing.g.dart';

@JsonSerializable()
class Listing<T> {
  @_ThingConverter()
  final List<T> children;

  Listing(this.children);

  factory Listing.fromJson(Map<String, dynamic> json) =>
      _$ListingFromJson(json);
}

class _ThingConverter<T> implements JsonConverter<T, Map<String, dynamic>> {
  const _ThingConverter();

  @override
  T fromJson(Map<String, dynamic> json) {
    final kind = json['kind'] as String;
    final data = json['data'] as Map<String, dynamic>;

    switch (kind) {
      case 't1':
        return TextComment.fromJson(data) as T;
      case 't2':
        return User.fromJson(data) as T;
      case 't3':
        return Link.fromJson(data) as T;
      case 't5':
        return Subreddit.fromJson(data) as T;
      case 'more':
        return MoreComment.fromJson(data) as T;
      default:
        return json as T;
    }
  }

  @override
  Map<String, dynamic> toJson(T object) => const {};
}
