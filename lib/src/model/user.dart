import 'package:json_annotation/json_annotation.dart';

import 'parsers/date_converter.dart';

part 'user.g.dart';

// Note: According to docs at https://github.com/reddit-archive/reddit/wiki/JSON
// this class does not implement Identifiable, name is not fullname. ID is
// normal ID though.
@JsonSerializable()
class User {
  final String name;
  final String id;
  final int commentKarma;
  final bool hasMail;
  final bool hasModMail;
  final bool hasVerifiedEmail;
  final int inboxCount;
  final bool isFriend;
  final bool isGold;
  final int linkKarma;
  final String modhash;

  @JsonKey(name: 'over_18')
  final bool overEighteen;

  @SecondsSinceEpochDateParser()
  final DateTime created;

  User(
      this.name,
      this.id,
      this.commentKarma,
      this.hasMail,
      this.hasModMail,
      this.hasVerifiedEmail,
      this.inboxCount,
      this.isFriend,
      this.isGold,
      this.linkKarma,
      this.modhash,
      this.overEighteen,
      this.created);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}
