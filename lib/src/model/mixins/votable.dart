mixin Votable {
  int get ups;
  int get downs;
  int get score;
  bool get likes;
}
