mixin Identifiable {
  String get id;
  String get name;
}
