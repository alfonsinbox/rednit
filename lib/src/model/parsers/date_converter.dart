import 'package:json_annotation/json_annotation.dart';

class SecondsSinceEpochDateParser implements JsonConverter<DateTime, double> {
  const SecondsSinceEpochDateParser();

  @override
  DateTime fromJson(double json) =>
      DateTime.fromMillisecondsSinceEpoch(json.toInt() * 1000, isUtc: true);

  @override
  double toJson(DateTime object) => object.millisecondsSinceEpoch / 1000.0;
}
