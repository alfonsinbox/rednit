enum ListingType { hot, recent, best, controversial, top }

extension ListingTypePaths on ListingType {
  String get path {
    switch (this) {
      case ListingType.hot:
        return 'hot';
      case ListingType.recent:
        return 'new';
      case ListingType.best:
        return 'best';
      case ListingType.controversial:
        return 'controversial';
      case ListingType.top:
        return 'top';
      default:
        throw Exception('This should never happen');
    }
  }
}
