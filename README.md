# rednit

A Dart wrapper for the Reddit API, built primarily for use with Flutter.

## Installing

Add rednit as a dependency in your `pubspec.yaml` file

```yaml
dependencies:
  rednit: <version>
```

## Usage

When using this package you will mainly be using a `RedditClient` instance, which should be created using the default constructor (future versions may include more than one constructor). This client instance has the necessary data members and methods used for accessing the Reddit API.

## Example

An example can be found in the [example](./example) folder showing how to use the `RedditClient`.
