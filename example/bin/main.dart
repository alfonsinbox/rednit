import 'package:http/http.dart';
import 'package:rednit/rednit.dart';

import 'secrets/reddit_constants.dart';

Future<void> main(List<String> arguments) async {
  final client = RedditClient(
    tokenStore: MemoryTokenStore(),
    credentials: ScriptAppCredentials(
      appVersion: '0.0.1',
      appName: 'Reddit Console',
      creatorUsername: RedditConstants.username,
      clientId: RedditConstants.clientId,
      clientSecret: RedditConstants.clientSecret,
      username: RedditConstants.username,
      password: RedditConstants.password,
    ),
    httpClient: Client(),
  );

  final me = await client.getMe();
  print(me.name);

  final otherUser = await client.getUser('spez');
  print(otherUser.commentKarma);
  print(otherUser.isFriend);

  final feed = await client.getFeed(ListingType.hot);
  print(feed.children
      .map((child) => '${child.createdUtc} ${child.title}')
      .join('\n'));

  final comments = await client.getComments(feed.children.first.id);
  print(comments.children.getRange(0, 3).map(textComment).join('\n'));

  await client.dispose();
}

String textComment(Comment comment) {
  if (comment is TextComment) {
    if (comment.replies.isNotEmpty) {
      return [
        '${'- ' * (comment.depth)}${comment.body.substring(0, 3)} // ${comment.parentId}',
        for (final child in comment.replies) textComment(child),
      ].join('\n');
    }
    return '${'- ' * (comment.depth)}${comment.body.substring(0, 3)} // ${comment.parentId}';
  }
  return '${'- ' * (comment.depth)}[more] // ${comment.parentId}';
}

class MemoryTokenStore extends TokenStore {
  @override
  String accessToken, deviceId, refreshToken;

  @override
  DateTime accessTokenExpiration = DateTime.fromMillisecondsSinceEpoch(0);

  @override
  Future<void> removeAccessToken() async {
    accessToken = null;
  }

  @override
  Future<void> removeAccessTokenExpiration() async {
    accessTokenExpiration = null;
  }

  @override
  Future<void> removeRefreshToken() async {
    refreshToken = null;
  }

  @override
  Future<void> setAccessToken(String accessToken) async {
    this.accessToken = accessToken;
  }

  @override
  Future<void> setAccessTokenExpiration(DateTime accessTokenExpiration) async {
    accessTokenExpiration = accessTokenExpiration;
  }

  @override
  Future<void> setDeviceId(String deviceId) async {
    this.deviceId = deviceId;
  }

  @override
  Future<void> setRefreshToken(String refreshToken) async {
    this.refreshToken = refreshToken;
  }
}
