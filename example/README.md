An example implementation of Rednit as a console application. Useful for developing new features for Rednit.

## Getting started

To try it out yourself, you first need to have a Reddit account and create a Reddit application (of type script) at https://www.reddit.com/prefs/apps/. This will give you access to a Client ID and a Client secret, which is required to use Reddit's APIs from a script.

Then you have to create the file `bin/secrets/reddit_constants.dart` and declare the class below, with your own client ID, client secret, username and password.

```dart
class RedditConstants {
  static const clientId = '<YOUR_CLIENT_ID>';
  static const clientSecret = '<YOUR_CLIENT_SECRET>';
  static const username = '<YOUR_REDDIT_USERNAME>';
  static const password = '<YOUR_REDDIT_PASSWORD>';
}
```
