## 0.3.0

**Breaking:** Create `getSubredditFeed` and remove `subreddit` parameter from `getFeed`.

Add dependency on json_serializable for parsing response from REST API.

Add methods for getting user, user submissions, and user comments.

## 0.2.0

**Breaking**

Replace `Credentials` class with two options for different use cases - `InstalledAppCredentials` and `ScriptAppCredentials`.

## 0.1.0+1

Update README.

## 0.1.0

Initial pub release.
