## Welcome

Thanks for considering contributing to rednit.  
The goal with this project is to create and publish a completely open sourced API Wrapper for Reddit. You can help by contributing in many different ways - submitting bug reports, requesting features, suggest ways to improve code quality, writing tests, or writing code for the app. All kinds of contributions is more than welcome!

This project is currently developed in parallel with the open sourced Reddit client [Rabbit](https://gitlab.com/alfonsinbox/rabbit).

Contributors can communicate using the GitLab issue tracker or our [Discord](https://discord.gg/sm8PA6s).

## Getting started

To contribute to the development of this project:

1. Create a fork of this project.
2. Clone your fork locally.
3. Do your changes in the fork.
4. Use the `example` to test your new features/changes.
5. Create a merge request. For info on how to make a merge request, see [New merge request from a fork](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-a-fork) by GitLab.

If you're just proposing a small or obvious fix, such as a clean up, typo correction or similar, it would be fine to just make a merge request without any discussion beforehand.  
For larger contributions such as new features, bug fixes, etc. there has to be an open issue in the issue tracker so specific users can be assigned as responsible for moving the issue forward. 
