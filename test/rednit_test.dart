import 'package:rednit/rednit.dart';
import 'package:mockito/mockito.dart' as mockito;
import 'package:test/test.dart' as test;
import 'package:http/http.dart' as http;

void main() {
  test.group('RedditClient lifecycle', () {
    test.test('Client sets device ID', () {
      final mockTokenStore = MockTokenStore();
      RedditClient(
        httpClient: MockHttpClient(),
        credentials: MockCredentials(),
        tokenStore: mockTokenStore,
      );
      mockito.verify(mockTokenStore.setDeviceId).called(1);
    });
    test.test('Client closes HTTP client', () async {
      final mockHttpClient = MockHttpClient();
      final client = RedditClient(
        httpClient: mockHttpClient,
        credentials: MockCredentials(),
        tokenStore: MockTokenStore(),
      );
      await client.dispose();
      mockito.verify(mockHttpClient.close());
    });
  });
}

class MockHttpClient extends mockito.Mock implements http.Client {}

class MockCredentials extends mockito.Mock implements Credentials {}

class MockTokenStore extends mockito.Mock implements TokenStore {}
